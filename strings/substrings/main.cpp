#include <QTextStream>
#include <QString>
#include <QStringView>

int main(void) {

    QTextStream out(stdout);

    QString str = { "The night train" };

    out << str.right(5) << Qt::endl;
    out << str.left(9) << Qt::endl;
    out << str.mid(4, 5) << Qt::endl;

    QString str2("The big apple");
    // QStringRef sub(&str2, 0, 7); // note: removed
    QStringView sub(str2.cbegin(), 7);

    out << sub.toString() << Qt::endl;

    return 0;
}

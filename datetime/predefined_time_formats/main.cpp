#include <QTextStream>
#include <QTime>

int main(void) {

    QTextStream out(stdout);

    QTime ct = QTime::currentTime();

    out << "The time is " << ct.toString(Qt::TextDate) << Qt::endl;
    out << "The time is " << ct.toString(Qt::ISODate) << Qt::endl;
    // out << "The time is " << ct.toString(Qt::SystemLocaleShortDate) << Qt::endl; // removed
    // out << "The time is " << ct.toString(Qt::SystemLocaleLongDate) << Qt::endl; // removed
    // out << "The time is " << ct.toString(Qt::DefaultLocaleShortDate) << Qt::endl; // removed
    // out << "The time is " << ct.toString(Qt::DefaultLocaleLongDate) << Qt::endl; // removed
    // out << "The time is " << ct.toString(Qt::SystemLocaleDate) << Qt::endl; // removed
    // out << "The time is " << ct.toString(Qt::LocaleDate) << Qt::endl; // removed
}

#include <QTextStream>
#include <QDate>

int main(void) {
    QTextStream out(stdout);

    QDate cd = QDate::currentDate();

    out << "Today is " << cd.toString(Qt::TextDate) << Qt::endl;
    out << "Today is " << cd.toString(Qt::ISODate) << Qt::endl;
    // out << "Today is " << cd.toString(Qt::SystemLocaleShortDate) << Qt::endl; // removed
    // out << "Today is " << cd.toString(Qt::SystemLocaleLongDate) << Qt::endl; // removed
    // out << "Today is " << cd.toString(Qt::DefaultLocaleShortDate) << Qt::endl; // removed
    // out << "Today is " << cd.toString(Qt::DefaultLocaleLongDate) << Qt::endl; // removed
    // out << "Today is " << cd.toString(Qt::SystemLocaleDate) << Qt::endl; // removed
    // out << "Today is " << cd.toString(Qt::LocaleDate) << Qt::endl; // removed
}
